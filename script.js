'use strict'
let firstNumber = +prompt("Enter first number");
let secondNumber = +prompt("Enter second number");
while (isNaN(firstNumber) || isNaN(secondNumber) || !firstNumber || !secondNumber){
    firstNumber = +prompt("Enter first number", firstNumber);
    secondNumber = +prompt("Enter second number", secondNumber);

}
let mathOperation = prompt("Enter math operation (+, -, *, /)");
function calc(firstNumber, secondNumber, mathOperation){
    if (mathOperation === "+"){
        console.log(firstNumber + secondNumber);
    }else if (mathOperation === "-"){
        console.log(firstNumber - secondNumber);
    }else if (mathOperation === "*"){
        console.log(firstNumber * secondNumber);
    }else{
        console.log(firstNumber / secondNumber);
    }
}
calc(firstNumber, secondNumber, mathOperation);
